/*
Date Added
- Jupyter notebook extension
- Display when each cell was added
- Useful e.g. when using Jupyter notebooks as scientific log books

NB: A lot of this is based on:
https://github.com/ipython-contrib/jupyter_contrib_nbextensions/tree/master/src/jupyter_contrib_nbextensions/nbextensions/execute_time

For everything else:

Copyright 2017 Steve Biggs

'Date Added' is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

'Date Added' is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with 'Date Added'.  If not, see http://www.gnu.org/licenses/.
*/


define([
    'require',
    'jquery',
    'moment',
    'base/js/events',
    'base/js/namespace'
], function(
    require,
    $,
    moment,
    events,
    Jupyter
) {
    'use strict'

    var options = {
        datetime_format: 'DD MMM YYYY, HH:mm'
    };

    function _on_load() {
        // This is effectively the main function
        _add_css('./DateAdded.css');  // Add area to stylesheet
        events.on('create.Cell', _log_time);  // Add callback function
        Jupyter.notebook.get_cells().forEach(_update_date_added_area);  // Re-add info after shutdown
    }

    function _add_css(url) {
        $('<link/>')
            .attr({
                rel: 'stylesheet',
                href: require.toUrl(url),
                type: 'text/css'
            })
            .appendTo('head');
    }

    function _log_time(evt, data) {
        var cell = data.cell;
        // Write date added information to cell metadata so that it persists after shutdown
        cell.metadata.DateAdded = {date_added: moment().toISOString()};
        // Write metadata to cell
        _update_date_added_area(cell);
    }

    function _update_date_added_area(cell){
        // Check that the metadata exists to avoid failure
        if (!cell.metadata.DateAdded ||
                !cell.metadata.DateAdded.date_added) {
            return $();
        }
        // Find the date added area
        var date_added_area = cell.element.find('.date_added_area');
        // Ensure that the date added area has been initialised
        if (date_added_area.length < 1) {
            date_added_area = $('<div/>')
                .addClass('date_added_area')
                .prependTo(cell.element.find('.inner_cell'));
        }
        // Write the metadata to the date added area
        date_added_area.text('Cell added: ' + _format_moment(moment(cell.metadata.DateAdded.date_added)));
    }

    function _format_moment(datetime) {
        // Format the date information according to the specifed options
        if (options.datetime_format) {
            return datetime.format(options.datetime_format);
        }
        return datetime;
    }

    return {
        // Run main function
        load_ipython_extension: _on_load
    };
});
